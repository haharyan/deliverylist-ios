//
//  BaseVC.swift
//  DeliveryList
//
//  Created by Ryan So on 12/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import UIKit
import RxSwift
import Stevia
import Then

class BaseVC: UIViewController {

  let bag = DisposeBag()
  
  var isFirstLoad = true
  
  override func viewDidLoad() {
    super.viewDidLoad()

    setNavBarBackground()
    
    self.setupUI()
    self.setupConstraints()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    if let nav = self.navigationController {
      if nav.viewControllers.count > 1 {
        self.setBackButton()
      }
    }
  }

  func setupUI() { }
  func setupConstraints() { }
}

//Shared Selector
@objc extension BaseVC {
  func popCurrentVC() {
    self.navigationController?.popViewController(animated: true)
  }
}

//Navigation Bar Related
extension BaseVC {
  func setNavBarBackground() {
    let height = self.navigationController?.navigationBar.bounds.height ?? (UIDevice.isEdgeToEdge ? 88 : 64)
    
    let gradient = CAGradientLayer().then {
      $0.locations = [0.0, 1.0]
      $0.startPoint = CGPoint(x: 0, y: 0)
      $0.endPoint = CGPoint(x: 1, y: 0)
      $0.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: height)
      $0.colors = [UIColor.dlOrange.withAlphaComponent(0.7).cgColor, UIColor.dlOrange.cgColor]
    }
    self.navigationController?.navigationBar.setBackgroundImage(gradient.getImage(), for: .default)
    self.navigationController?.navigationBar.shadowImage = UIImage()
    self.navigationController?.navigationBar.isTranslucent = false
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSAttributedString.Key.foregroundColor: UIColor.white,
      NSAttributedString.Key.kern: 0.5
    ]
  }
  
  func setBackButton() {
    let leftBarButton = UIButton(type: .system).then {
      $0.setImage(Asset.Icon.iconBackArrow.image.withRenderingMode(.alwaysOriginal), for: .normal)
      $0.imageEdgeInsets = UIEdgeInsets(top: 0, left: -22, bottom: 0, right: 0)
      $0.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
      $0.addTarget(self, action: #selector(popCurrentVC), for: .touchUpInside)
    }
    self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: leftBarButton)
  }
}
