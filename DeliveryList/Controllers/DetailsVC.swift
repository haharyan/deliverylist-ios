//
//  DetailsVC.swift
//  DeliveryList
//
//  Created by Ryan So on 12/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import UIKit
import RxSwift
import Stevia
import MapKit

class DetailsVC: BaseVC {

  private var mapView: MKMapView!
  private var imageView: UIImageView!
  
  private var infoView: UIView!
  private var descLabel: UILabel!
  private var iconImageView: UIImageView!
  private var locationLabel: UILabel!
  
  var item: DeliveryItem?

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()

    self.title = S.deliveryDetails
    self.view.backgroundColor = UIColor.white
    
    setItemDetails()
  }

  override func setupUI() {
    mapView = MKMapView().then {
      $0.showsUserLocation = true
    }
    
    imageView = UIImageView().then {
      $0.clipsToBounds = true
      $0.contentMode = .scaleAspectFill
    }
    
    infoView = UIView()
    descLabel = UILabel()
    iconImageView = UIImageView(image: Asset.Icon.iconPin.image.withRenderingMode(.alwaysTemplate)).then {
      $0.tintColor = UIColor.dlOrange
      $0.contentMode = .scaleAspectFit
      $0.clipsToBounds = true
    }
    locationLabel = UILabel().then {
      $0.font = UIFont.systemFont(ofSize: 12)
      $0.textColor = UIColor.lightGray
    }
    
    self.view.sv(
      mapView,
      imageView,
      infoView.sv(
        descLabel,
        iconImageView, locationLabel
      )
    )
  }

  override func setupConstraints() {
    mapView.width(100%).centerHorizontally().top(0)
    mapView.Height == 90 % mapView.Width
    
    imageView.width(84).heightEqualsWidth().left(14)
    imageView.Top == mapView.Bottom + 14
    
    infoView.right(14)
    infoView.CenterY == imageView.CenterY
    infoView.Left == imageView.Right + 14
    
    descLabel.height(15).fillHorizontally().top(0)
    iconImageView.width(10).height(15).left(0)
    iconImageView.CenterY == locationLabel.CenterY
    locationLabel.height(15).right(0).bottom(0)
    locationLabel.Top == descLabel.Bottom + 4
    locationLabel.Left == iconImageView.Right + 4
  }
}

// class selector
extension DetailsVC {
  private func setItemDetails() {
    if let lat = item?.location?.lat, let lng = item?.location?.lng {
      let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
      
      let annotation = MKPointAnnotation().then {
        $0.coordinate = coordinate
      }
      mapView.addAnnotation(annotation)
      mapView.setRegion(MKCoordinateRegion(center: coordinate, latitudinalMeters: 500, longitudinalMeters: 500), animated: false)
    }
    
    imageView.loadImage(urlString: item?.imageUrl)
    descLabel.attributedText = item?.desc?.highlightKeyword()
    locationLabel.text = item?.location?.address
  }
}
