//
//  ListVC.swift
//  DeliveryList
//
//  Created by Ryan So on 12/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import UIKit
import RxDataSources
import RxSwift
import Stevia
import Reachability

class ListVC: BaseVC {
  
  private var refreshControl: UIRefreshControl!
  private var tableView: UITableView!
  private var reminderView: ReminderView!

  //Data
  private var items = BehaviorSubject<[DeliveryItem]>(value: [])
  
  //Pagination
  private var currentPage = 1
  private var isLoading = false
  private var isLoadMore = false
  private var isLastPage = false
  private var isReachable = true
  
  private let reachability = Reachability()!
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()

    self.title = S.thingsToDeliver
    self.view.backgroundColor = UIColor.white
    
    do {
      try reachability.startNotifier()
    } catch {
      //do nothing
    }
    
    items.asObservable().bind(to: tableView.rx.items(cellIdentifier: DeliveryItemCell.cellId)) { index, item, cell in
      guard let cell = cell as? DeliveryItemCell else { return }
      
      cell.imageUrl = item.imageUrl
      cell.title = item.desc
      cell.subtitle = item.location?.address
      
    }.disposed(by: bag)
    
    refreshControl.rx.controlEvent(.valueChanged).subscribe(onNext: { [weak self] (_) in
      guard let ss = self else { return }
      
      if ss.refreshControl.isRefreshing {
        ss.currentPage = 1
        ss.isLastPage = false
        ss.apiGetDeliveryItems()
      }
    }).disposed(by: bag)
    
    tableView.rx.didScroll.subscribe(onNext: { [weak self] (_) in
      guard let ss = self, ss.isReachable else { return }
      
      let reloadDistance = UIScreen.main.bounds.height / 2
      let contentHeight = ss.tableView.contentSize.height
      let frameHeight = ss.tableView.frame.height
      let bottomEdge = ss.tableView.contentOffset.y + frameHeight
      
      if contentHeight > frameHeight, bottomEdge >= contentHeight - reloadDistance, !ss.isLoadMore, !ss.isLastPage {
        ss.isLoadMore = true
        ss.currentPage += 1
        ss.apiGetDeliveryItems()
      }
      
    }).disposed(by: bag)
    
    items.onNext(RealmServices.shared.getDeliveryItems())
    
    NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidDisappear(animated)
   
    if isFirstLoad {
      isFirstLoad = false
      apiGetDeliveryItems()
    }
  }

  override func setupUI() {
    refreshControl = UIRefreshControl()
    tableView = UITableView().then {
      $0.backgroundColor = UIColor.dlLightGray
      $0.delegate = self
      $0.register(DeliveryItemCell.self, forCellReuseIdentifier: DeliveryItemCell.cellId)
      $0.estimatedSectionHeaderHeight = 0
      $0.estimatedSectionFooterHeight = 0
      $0.estimatedRowHeight = 0
      $0.rowHeight = 84
      $0.separatorStyle = .none
      $0.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 10))
      
      $0.refreshControl = refreshControl
    }
    reminderView = ReminderView()
    
    self.view.sv(
      tableView,
      reminderView
    )
  }

  override func setupConstraints() {
    tableView.fillContainer()
  }
}

// API related
extension ListVC {
  private func apiGetDeliveryItems() {
    guard !isLoading else { return }
    isLoading = true
    
    reminderView.show()
    
    let itemsPerPage = DLConfigs.ITEMS_PER_PAGE
    let param: [String: Any] = [
      "offset": (currentPage - 1) * itemsPerPage,
      "limit": itemsPerPage
    ]
    
    APIProvider.rx
      .request(.getDeliveryItems(param: param))
      .filterSuccessfulStatusCodes()
      .observeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
      .mapResponseArray(DeliveryItem.self)
      .observeOn(MainScheduler.instance)
      .subscribe(onSuccess: { [weak self] (response) in
        guard let ss = self else { return }
        
        ss.reminderView.dismiss()
        ss.isLoading = false
        
        guard let items = response.objects as? [DeliveryItem] else {
          return
        }
        
        RealmServices.shared.cacheDeliveryItems(items: items)
        
        if ss.refreshControl.isRefreshing {
          ss.refreshControl.endRefreshing()
          ss.items.onNext(RealmServices.shared.getDeliveryItems())
        } else if ss.isLoadMore {
          ss.isLoadMore = false
          ss.isLastPage = items.count < itemsPerPage
          
          if var currentItems = try? ss.items.value() {
            currentItems.append(contentsOf: items)
            ss.items.onNext(currentItems)
          }
        } else {
          ss.items.onNext(items)
        }
        }, onError: { [weak self] (error) in
          guard let ss = self else { return }
          UIApplication.shared.isNetworkActivityIndicatorVisible = false
          print("error: \(error.localizedDescription)")
          
          ss.reminderView.dismiss()
          ss.isLoading = false
          
          if ss.refreshControl.isRefreshing {
            ss.refreshControl.endRefreshing()
          } else if ss.isLoadMore {
            ss.isLoadMore = false
            ss.currentPage -= 1
          }
          
          ss.items.onNext(RealmServices.shared.getDeliveryItems())
      }).disposed(by: bag)
  }
}

// class selector
extension ListVC {
  @objc func reachabilityChanged(note: Notification) {
    let reachability = note.object as! Reachability
    isReachable = reachability.connection != .none
  }
}

// UITableViewDelegate
extension ListVC: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let targetVC = DetailsVC().then {
      $0.item = try? items.value()[indexPath.row]
    }
    self.navigationController?.pushViewController(targetVC, animated: true)
  }
}
