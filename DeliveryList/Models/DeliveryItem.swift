//
//  DeliveryItem.swift
//  DeliveryList
//
//  Created by Ryan So on 12/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import ObjectMapper
import RealmSwift
import Realm

class DeliveryItem: Object, Mappable {

  @objc dynamic var id = 0
  @objc dynamic var desc: String? = nil
  @objc dynamic var imageUrl: String? = nil
  @objc dynamic var location: Location? = nil

  required convenience init?(map: Map) { self.init() }

  override static func primaryKey() -> String? {
    return "id"
  }
  
  // Mappable
  func mapping(map: Map) {
    id <- map["id"]
    desc <- map["description"]
    imageUrl <- map["imageUrl"]
    location <- map["location"]
  }
}
