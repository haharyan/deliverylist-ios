//
//  Location.swift
//  DeliveryList
//
//  Created by Ryan So on 12/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import ObjectMapper
import RealmSwift
import Realm

class Location: Object, Mappable {

  @objc dynamic var id = 0
  @objc dynamic var lat: Double = 0.0
  @objc dynamic var lng: Double = 0.0
  @objc dynamic var address: String? = nil

  required convenience init?(map: Map) { self.init() }
  
  override static func primaryKey() -> String? {
    return "id"
  }
  
  // Mappable
  func mapping(map: Map) {
    lat <- map["lat"]
    lng <- map["lng"]
    address <- map["address"]
  }
}
