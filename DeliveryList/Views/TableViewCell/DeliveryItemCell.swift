//
//  DeliveryItemCell.swift
//  DeliveryList
//
//  Created by Ryan So on 13/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import UIKit
import RxSwift
import Stevia

class DeliveryItemCell: UITableViewCell {

  static var cellId = "DeliveryItemCell"
  
  private var cellContentView: UIView!
  private var itemImageView: UIImageView!
  
  private var infoView: UIView!
  private var titleLabel: UILabel!
  private var iconImageView: UIImageView!
  private var subtitleLabel: UILabel!
  
  var imageUrl: String? {
    didSet {
      itemImageView.loadImage(urlString: imageUrl)
    }
  }
  var title: String? {
    didSet {
      titleLabel.attributedText = title?.highlightKeyword()
    }
  }
  var subtitle: String? {
    didSet {
      subtitleLabel.text = subtitle
    }
  }

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    self.selectionStyle = .none
    self.backgroundColor = UIColor.dlLightGray
    
    cellContentView = UIView().then {
      $0.backgroundColor = UIColor.white
    }
    
    itemImageView = UIImageView().then {
      $0.contentMode = .scaleAspectFill
      $0.clipsToBounds = true
    }
    
    infoView = UIView()
    titleLabel = UILabel()
    iconImageView = UIImageView(image: Asset.Icon.iconPin.image.withRenderingMode(.alwaysTemplate)).then {
      $0.tintColor = UIColor.dlOrange
      $0.contentMode = .scaleAspectFit
      $0.clipsToBounds = true
    }
    subtitleLabel = UILabel().then {
      $0.font = UIFont.systemFont(ofSize: 12)
      $0.textColor = UIColor.lightGray
    }
    
    self.sv(
      cellContentView.sv(
        itemImageView,
        infoView.sv(
          titleLabel,
          iconImageView, subtitleLabel
        )
      )
    )
    
    self.setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
}

extension DeliveryItemCell {
  func setupConstraints() {
    cellContentView.fillHorizontally(m: 10).top(10).bottom(0)
    itemImageView.fillVertically(m: 8).left(8)
    itemImageView.Width == itemImageView.Height
    
    infoView.centerVertically().right(8)
    infoView.Left == itemImageView.Right + 10
    titleLabel.height(15).fillHorizontally().top(0)
    iconImageView.width(10).height(15).left(0)
    iconImageView.CenterY == subtitleLabel.CenterY
    subtitleLabel.height(15).right(0).bottom(0)
    subtitleLabel.Top == titleLabel.Bottom + 4
    subtitleLabel.Left == iconImageView.Right + 4
  }
}
