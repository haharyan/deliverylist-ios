//
//  ReminderView.swift
//  DeliveryList
//
//  Created by Ryan So on 14/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import UIKit
import RxSwift
import Stevia

class ReminderView: UIView {

  private var titleLabel: UILabel!

  private var dotCount = 0
  private var timer: Timer?
  
  private var isAnimating = false
  
  convenience init() {
    self.init(frame: CGRect.zero)

    self.alpha = 0
    self.isHidden = true
    self.backgroundColor = UIColor.black
    
    setupUI()
    setupConstraints()
    
    initTime()
  }

  func setupUI() {
    titleLabel = UILabel().then {
      $0.font = UIFont.systemFont(ofSize: 14)
      $0.text = S.loading
      $0.textColor = UIColor.white
    }
    
    self.sv(titleLabel)
  }

  func setupConstraints() {
    titleLabel.centerVertically().left(17).right(10)
  }
  
  override func didMoveToSuperview() {
    guard superview != nil else { return }
    self.width(96).height(32).centerHorizontally().bottom(0)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    self.layer.cornerRadius = self.frame.height / 2
  }
}

// private function
extension ReminderView {
  @objc private func animation() {
    if dotCount == 3 {
      dotCount = 0
    }
    dotCount = min(3, dotCount + 1)
    
    var dot = ""
    for _ in 0 ..< dotCount {
      dot.append(".")
    }
    
    titleLabel.text = String(format: "%@%@", S.loading, dot)
  }
  
  private func initTime() {
    timer = nil
    timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(animation), userInfo: nil, repeats: true)
  }
}

// public function
extension ReminderView {
  func show() {
    isAnimating = true
    
    self.isHidden = false
    self.bottomConstraint?.constant = -60
    
    UIView.animate(withDuration: 0.3, animations: {
      self.alpha = 1
      self.superview?.layoutIfNeeded()
    }, completion: { (Bool) in
      self.isAnimating = false
    })
    
    initTime()
    RunLoop.current.add(timer!, forMode: .common)
  }
  
  func dismiss() {
    if isAnimating {
      CommonHelper.runAfterDelay(0.5) {
        self.hideReminder()
      }
    } else {
      self.hideReminder()
    }
  }
  
  private func hideReminder() {
    isAnimating = true
    self.bottomConstraint?.constant = 0
    
    UIView.animate(withDuration: 0.3, animations: {
      self.alpha = 0
      self.superview?.layoutIfNeeded()
    }, completion: { (Bool) in
      self.isHidden = true
      self.timer?.invalidate()
      self.dotCount = 0
      self.isAnimating = false
    })
  }
}
