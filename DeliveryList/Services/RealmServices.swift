//
//  RealmServices.swift
//  DeliveryList
//
//  Created by Ryan So on 12/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import UIKit
import RealmSwift

class RealmServices {
  static var shared: RealmServices = RealmServices()
  
  lazy var realm: Realm = {
    var config = Realm.Configuration()
    config.fileURL = config.fileURL!.deletingLastPathComponent().appendingPathComponent("delivery_items.realm")
    Realm.Configuration.defaultConfiguration = config
    
    let realm = try! Realm()
    return realm
  }()
  
  func getDeliveryItems() -> [DeliveryItem] {
//    print("realm File: \(Realm.Configuration().fileURL)")
    
    let results = realm.objects(DeliveryItem.self).sorted(byKeyPath: "id", ascending: true)
    return [DeliveryItem](results)
  }
  
  func cacheDeliveryItems(items: [DeliveryItem]) {
    try! realm.write {
      items.enumerated().forEach({ (index, item) in
        item.id = index
        item.location?.id = index
        realm.create(DeliveryItem.self, value: item, update: true)
      })
    }
  }
}
