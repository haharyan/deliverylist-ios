//
//  DLServices.swift
//  DeliveryList
//
//  Created by Ryan So on 12/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import ObjectMapper
import Alamofire

class DefaultAlamofireManager: Alamofire.SessionManager {
  static let shared: DefaultAlamofireManager = {
    let configuration = URLSessionConfiguration.default
    configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
    configuration.timeoutIntervalForRequest = 10
    configuration.timeoutIntervalForResource = 20
    configuration.requestCachePolicy = .useProtocolCachePolicy
    return DefaultAlamofireManager(configuration: configuration)
  }()
}

enum DLServices {
  case getDeliveryItems(param: [String: Any])
}

extension DLServices: TargetType {
  
  var baseURL: URL {
    switch self {
    default:
      return URL(string: DLConfigs.HOST)!
    }
  }
  
  var path: String {
    switch self {
    case .getDeliveryItems:
      return "deliveries"
    }
  }
  
  var method: Moya.Method {
    switch self {
    default:
      return .get
    }
  }
  
  var sampleData : Data {
    return "".data(using: String.Encoding.utf8)!
  }
  
  var task: Task {
    switch(self) {
    case .getDeliveryItems(let param):
      return .requestParameters(parameters: param, encoding: URLEncoding.queryString)
    }
  }
  
  var headers: [String : String]? {
    return [:]
  }
}

var requestClosure: MoyaProvider<DLServices>.RequestClosure = { endpoint, done in
  let url = endpoint.url
  
  var request = try? endpoint.urlRequest()
  
  if request != nil {
    done(.success(request!))
  }
}

let activityIndicatorPlugin: PluginType = NetworkActivityPlugin() { (type, target) in
  switch type {
  case .began:
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
  case .ended:
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }
}

let pluginLists = [NetworkLoggerPlugin(verbose: true), activityIndicatorPlugin]
let APIProvider = MoyaProvider<DLServices>(requestClosure: requestClosure, manager: DefaultAlamofireManager.shared, plugins: pluginLists)
