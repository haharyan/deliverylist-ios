//
//  DLConfigs.swift
//  DeliveryList
//
//  Created by Ryan So on 12/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import Foundation

typealias S = L10n

struct DLConfigs {
  static var HOST = "https://mock-api-mobile.dev.lalamove.com"
  static var ITEMS_PER_PAGE = 20
}
