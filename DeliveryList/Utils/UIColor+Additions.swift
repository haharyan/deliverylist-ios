//
//  UIColor+Additions.swift
//  DeliveryList
//
//  Created by Ryan So on 12/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import UIKit

extension UIColor {
  @nonobjc class var dlOrange: UIColor {
    return UIColor(red: 236.0 / 255.0, green: 80.0 / 255.0, blue: 27.0 / 255.0, alpha: 1.0)
  }
  
  @nonobjc class var dlYellow: UIColor {
    return UIColor(red: 252.0 / 255.0, green: 142.0 / 255.0, blue: 9.0 / 255.0, alpha: 1.0)
  }
  
  @nonobjc class var dlLightGray: UIColor {
    return UIColor(white: 236.0 / 255.0, alpha: 1.0)
  }
}
