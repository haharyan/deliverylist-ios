//
//  DLUtils.swift
//  DeliveryList
//
//  Created by Ryan So on 12/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import UIKit
import Kingfisher

extension CAGradientLayer {
  func getImage() -> UIImage? {
    var gradientImage: UIImage?
    
    UIGraphicsBeginImageContext(self.frame.size)
    if let context = UIGraphicsGetCurrentContext() {
      self.render(in: context)
      gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
    }
    UIGraphicsEndImageContext()
    return gradientImage
  }
}

extension String {
  func highlightKeyword() -> NSMutableAttributedString {
    var attributedString = self.attributed.font(UIFont.boldSystemFont(ofSize: 14)).textColor(UIColor.black)
    
    ["deliver ", " to "].forEach {
      let range = (self.lowercased() as NSString).range(of: $0.lowercased())
      attributedString = attributedString.font(UIFont.systemFont(ofSize: 14), range: range)
    }
    
    return attributedString
  }
}

extension UINavigationController {
  override open var childForStatusBarStyle: UIViewController? {
    return visibleViewController
  }
}

extension UIImageView {
  func loadImage(urlString: String?) {
    guard let urlString = urlString, let url = URL(string: urlString) else { return }
    self.kf.setImage(with: ImageResource(downloadURL: url, cacheKey: nil), placeholder: Asset.Icon.iconPlaceholder.image, options: [.transition(.fade(0.5))])
  }
}
