//
//  ObservableResponseObject.swift
//  DeliveryList
//
//  Created by Ryan So on 12/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import ObjectMapper
import Moya_ObjectMapper

public struct ResponseObject {
  var response: Response?
  var object: BaseMappable?
}

public struct ResponseArray {
  var response: Response?
  var objects: [BaseMappable]?
}

public struct ProgressResponseObject {
  var response: ProgressResponse?
  var object: BaseMappable?
}

public struct ProgressResponseArray {
  var response: ProgressResponse?
  var objects: [BaseMappable]?
}

/// Extension for processing Responses into Mappable objects through ObjectMapper
public extension ObservableType where E == Response {
  
  public func mapResponseObject<T: BaseMappable>(_ type: T.Type) -> Observable<ResponseObject> {
    return flatMap { response -> Observable<ResponseObject> in
      return Observable.just(ResponseObject(response: response, object: try? response.mapObject(T.self)))
    }
  }
  
  public func mapResponseArray<T: BaseMappable>(_ type: T.Type) -> Observable<ResponseArray> {
    return flatMap { response -> Observable<ResponseArray> in
      return Observable.just(ResponseArray(response: response, objects: try? response.mapArray(T.self)))
    }
  }
}

public extension PrimitiveSequence where TraitType == SingleTrait, ElementType == Response {
  
  public func mapResponseObject<T: BaseMappable>(_ type: T.Type) -> Single<ResponseObject> {
    return flatMap { response -> Single<ResponseObject> in
      return Single.just(ResponseObject(response: response, object: try? response.mapObject(T.self)))
    }
  }
  
  public func mapResponseArray<T: BaseMappable>(_ type: T.Type) -> Single<ResponseArray> {
    return flatMap { response -> Single<ResponseArray> in
      return Single.just(ResponseArray(response: response, objects: try? response.mapArray(T.self)))
    }
  }
}

extension ObservableType where E == ProgressResponse {
  
  public func mapResponseObject<T: BaseMappable>(_ type: T.Type) -> Observable<ProgressResponseObject> {
    return flatMap { response -> Observable<ProgressResponseObject> in
      return Observable.just(ProgressResponseObject(response: response, object: try! response.response?.mapObject(T.self)))
    }
  }
  
  public func mapResponseArray<T: BaseMappable>(_ type: T.Type) -> Observable<ProgressResponseArray> {
    return flatMap { response -> Observable<ProgressResponseArray> in
      return Observable.just(ProgressResponseArray(response: response, objects: try! response.response?.mapArray(T.self)))
    }
  }
}
