//
//  UIDeviceUtils.swift
//  DeliveryList
//
//  Created by Ryan So on 12/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import UIKit
import DeviceKit

extension UIDevice {
  static public var isPhone5Size: Bool {
    let device = Device()
    
    if device.isSimulator {
      return UIScreen.main.bounds.height == 568
    }
    return device == .iPhone5 || device == .iPhone5c || device == .iPhone5s || device == .iPhoneSE
  }
  
  static public var isPhoneSize: Bool {
    let device = Device()
    
    if device.isSimulator {
      return UIScreen.main.bounds.height == 667
    }
    return device == .iPhone6 || device == .iPhone6s || device == .iPhone7 || device == .iPhone8
  }
  
  static public var isPhonePlusSize: Bool {
    let device = Device()
    
    if device.isSimulator {
      return UIScreen.main.bounds.height == 736
    }
    return device == .iPhone6Plus || device == .iPhone6sPlus || device == .iPhone7Plus || device == .iPhone8Plus
  }
  
  static public var isPhoneX: Bool {
    let device = Device()
    
    if device.isSimulator {
      return UIScreen.main.bounds.height == 812
    }
    return device == .iPhoneX || device == .iPhoneXs
  }
  
  static public var isPhoneXsMax: Bool {
    let device = Device()
    
    if device.isSimulator {
      return UIScreen.main.bounds.height == 896
    }
    return device == .iPhoneXsMax
  }
  
  static public var isPhoneXr: Bool {
    let device = Device()
    
    if device.isSimulator {
      return UIScreen.main.bounds.height == 896
    }
    return device == .iPhoneXr
  }
  
  static public var isEdgeToEdge: Bool {
    return isPhoneX || isPhoneXsMax || isPhoneXr
  }
}
