//
//  CommonHelper.swift
//  DeliveryList
//
//  Created by Ryan So on 14/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import UIKit

class CommonHelper : NSObject {
  static func runAfterDelay(_ delay: TimeInterval, block: @escaping ()->()) {
    let time = DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
    DispatchQueue.main.asyncAfter(deadline: time, execute: block)
  }
}
