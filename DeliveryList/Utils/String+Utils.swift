//
//  String+Utils.swift
//  DeliveryList
//
//  Created by Ryan So on 13/2/2019.
//  Copyright © 2019 Ryan So. All rights reserved.
//

import UIKit

extension String {
  var attributed: NSMutableAttributedString {
    return NSMutableAttributedString(string: self)
  }
}

extension NSAttributedString {
  var attributed: NSMutableAttributedString {
    return NSMutableAttributedString(attributedString: self)
  }
}

extension NSMutableAttributedString {
  func font(_ font: UIFont) -> NSMutableAttributedString {
    self.addAttribute(NSAttributedString.Key.font, value: font, range: NSMakeRange(0, self.length))
    return self
  }
  func font(_ font: UIFont, range: NSRange) -> NSMutableAttributedString {
    self.addAttribute(NSAttributedString.Key.font, value: font, range: range)
    return self
  }
  
  func kern(_ value: Double) -> NSMutableAttributedString {
    self.addAttribute(NSAttributedString.Key.kern, value: value, range: NSMakeRange(0, self.length))
    return self
  }
  
  func textColor(_ color: UIColor) -> NSMutableAttributedString {
    self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSMakeRange(0, self.length))
    return self
  }
  func textColor(_ color: UIColor, range: NSRange) -> NSMutableAttributedString {
    self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    return self
  }
  
  func underlineStyle(_ value: NSUnderlineStyle) -> NSMutableAttributedString {
    self.addAttribute(NSAttributedString.Key.underlineStyle, value: value.rawValue, range: NSMakeRange(0, self.length))
    return self
  }
  
  func underlineColor(_ value: UIColor) -> NSMutableAttributedString {
    self.addAttribute(NSAttributedString.Key.underlineColor, value: value, range: NSMakeRange(0, self.length))
    return self
  }
  
  func lineSpacing(_ value: CGFloat, alignment: NSTextAlignment = .center) -> NSMutableAttributedString {
    let paragraphStyle = NSMutableParagraphStyle().then {
      $0.lineSpacing = value
      $0.alignment = alignment
    }
    self.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, self.length))
    return self
  }
  
  func textAlignment(alignment: NSTextAlignment) -> NSMutableAttributedString {
    let paragraphStyle = NSMutableParagraphStyle().then {
      $0.alignment = alignment
    }
    self.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, self.length))
    return self
  }
}
